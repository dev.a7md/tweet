const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const testSchema = mongoose.Schema({
  peu: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  }
});

testSchema.pre('save', function (next) {
  const user = this;
  bcrypt.genSalt(10, function (err, salt) {
    if (err) {
      return next(err);
    }
    bcrypt.hash(user['password'], salt, function (err, hash) {
      if (err) {
        return next(err);
      }
      user['password'] = hash;
      next();
    })
  })
});

testSchema.methods.comparePassword = function (password, cb) {
  bcrypt.compare(password, this.password, function (err, isMatch) {
    if (err) {
      cb(err);
    }
    cb(null, isMatch)
  })
};

module.exports = mongoose.model('test', testSchema);
