const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const userSchema = mongoose.Schema({
  name: String,
  phone: Number,
  email: String,
  username: {
    type: String,
    unique: true
  },
  password: String
});

userSchema.pre('save', function (next) {
  const user = this;
  bcrypt.genSalt(10, function (err, salt) {
    if (err) {
      return next(err);
    }
    bcrypt.hash(user['password'], salt, function (err, hash) {
      if (err) {
        return next(err);
      }
      user['password'] = hash;
      next();
    })
  })
});

userSchema.methods.comparePassword = function (password, cb) {
  bcrypt.compare(password, this.password, function (err, isMatch) {
    if (err) {
      cb(err);
    }
    cb(null, isMatch)
  })
};

module.exports = mongoose.model('user', userSchema);
