const express = require('express');
const router = express.Router();
const passport = require('passport');
const User = require('../models/userModel');
const Test = require('../models/test');

router.get('/test', (req, res) => {
  console.log(req.user);
  console.log(req['user']);
  console.log(req.session);
  console.log(req['session']);
  res.end('Test request ended');
});

router.post('/signup', async function (req, res, next) {
  const user = new User({
    name: req.body.name,
    email: req.body.email,
    phone: req.body.phone,
    username: req.body.username,
    password: req.body.password
  });
  const savedUser = await user.save();

  // console.log(JSON.parse(req.user));
  res.send(savedUser);
});

// router.post('/signin', (req, res, next) => {
//     passport.authenticate('local', {session: true}, (err, user, info) => {
//       if (err) {
//         return next(err);
//       }
//       if (!user) {
//         return res.status(401).end(info['message']);
//       }
//       return res.send(user);
//     })(req, res, next)
//   }
// );

router.post('/signin', passport.authenticate('local', {}), (req, res) => {
  console.log('signin', req.session);
  res.send(req.user);
});

router.get('/current_user', (req, res) => {
  console.log('Reached current user');
  if (req.user) {
    return res.send(req.user);
  }
  res.status(404).send('You have to login');
});

module.exports = router;
