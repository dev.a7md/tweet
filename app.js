const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const session = require('cookie-session');
const logger = require('morgan');
const mongoose = require('mongoose');
const passport = require('passport');

const app = express();
mongoose.connect('mongodb://localhost:27017/twitter', {useNewUrlParser: true})
  .then(() => console.log('Connected'))
  .catch(() => console.log('Connection Error'));
require('./services/passport');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(session({
  name: 'session',
  keys: ['asdf1234'],
  maxAge: 1000 * 60
}));
app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());
app.use(passport.session());

app.use('/api', require('./routes/index'));
app.use('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'public', 'index.html'))
});

// app.use(function (req, res, next) {
//   res.setHeader('Access-Control-Allow-Origin', '*');
//   res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
//   res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PATCH, DELETE, OPTIONS');
//   next();
// });
// app.use('/', require('./routes/index'));
//
// // catch 404 and forward to error handler
// app.use(function (req, res, next) {
//   return res.render('index');
// });

module.exports = app;
