import {Component} from '@angular/core';
import {faSearch, faUserFriends} from '@fortawesome/free-solid-svg-icons';
import {faComment} from '@fortawesome/free-regular-svg-icons';
import {faTwitter} from '@fortawesome/free-brands-svg-icons';

@Component({
  selector: 'app-fullview',
  templateUrl: './fullView.component.html',
  styleUrls: ['./fullView.component.css']
})

export class FullViewComponent {
  faSearch = faSearch;
  faUserFriends = faUserFriends;
  faComment = faComment;
  faTwitter = faTwitter;
}
