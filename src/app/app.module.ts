import {ErrorHandler, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';

import {AppComponent} from './app.component';
import {LandingComponent} from './landing/landing.component';
import {FullViewComponent} from './landing/fullView/fullView.component';
import {FooterComponent} from './landing/footer/footer.component';
import {LoginComponent} from './login/login.component';
import {SignupComponent} from './signup/signup.component';
import {LoginError} from './login/login.error';
import {FeedComponent} from './feed/feed.component';

const appRoutes: Routes = [
  {path: '', component: LandingComponent},
  {path: 'login', component: LoginComponent},
  {path: 'signup', component: SignupComponent},
  {path: 'feed', component: FeedComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    FullViewComponent,
    FooterComponent,
    LoginComponent,
    SignupComponent,
    FeedComponent
  ],
  imports: [
    BrowserModule,
    FontAwesomeModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    {provide: ErrorHandler, useClass: LoginError}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
