import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class SignupService {
  user;

  constructor(private httpClient: HttpClient) {
  }

  submitUser(user) {
    this.user = user;
    return this.httpClient.post('http://localhost:3000/api/signup', this.user);
  }
}
