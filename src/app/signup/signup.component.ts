import {Component, OnInit, ViewChild} from '@angular/core';
import {faTwitter} from '@fortawesome/free-brands-svg-icons';
import {FormControl, FormGroup, NgForm, Validators} from '@angular/forms';
import {SignupService} from './signup.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
  providers: [SignupService]
})

export class SignupComponent implements OnInit {
  @ViewChild('f') previewFrom: NgForm;
  signupForm: FormGroup;

  status = 'Phone';
  faTwitter = faTwitter;
  user;
  toSubmit = false;
  name;
  email;
  phone;

  constructor(private signupService: SignupService, private router: Router) {
  }

  ngOnInit() {
    this.signupForm = new FormGroup({
      'name': new FormControl(null, Validators.required),
      'phone': new FormControl(null, Validators.required),
      'email': new FormControl(null, [Validators.required, Validators.email])
    });
  }

  toggle() {
    this.status === 'Phone' ? this.status = 'Email' : this.status = 'Phone';
  }

  goToSubmit() {
    this.toSubmit = !this.toSubmit;
    this.phone = this.signupForm.value.phone;
    this.email = this.signupForm.value.email;
    this.name = this.signupForm.value.name;
  }

  onSubmit() {
    this.user = {
      name: this.previewFrom.form.value.name,
      [this.status.toLowerCase()]: this.previewFrom.form.value[this.status.toLowerCase()]
    };
    this.previewFrom.resetForm();

    this.signupService.submitUser(this.user).subscribe(
      res => {
        console.log(res);
        this.router.navigate(['/feed']).then(d => console.log(d));
      }
    );
  }

  back() {
    this.toSubmit = !this.toSubmit;
    this.signupForm.patchValue({
      name: this.name,
      [this.status.toLowerCase()]: this.status.toLowerCase() === 'phone' ? this.phone : this.email
    });
  }
}
