import {Injectable} from '@angular/core';
import {LoginModel} from './login.model';
import {HttpClient} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {Observable, throwError} from 'rxjs';

@Injectable()
export class LoginService {
  private user: LoginModel;

  constructor(private http: HttpClient) {
  }

  sendUser(user: LoginModel) {
    this.user = user;
    return this.http.post('http://localhost:3000/api/signin', this.user)
      .pipe(
        catchError((err: Response) => {
          return throwError(err['error']);
        })
      );
  }
}
