import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

import {faTwitter} from '@fortawesome/free-brands-svg-icons/faTwitter';
import {LoginService} from './login.service';
import {LoginModel} from './login.model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [LoginService]
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  faTwitter = faTwitter;
  user: LoginModel;

  constructor(private loginService: LoginService, private router: Router) {
  }

  ngOnInit() {
    this.form = new FormGroup({
      peu: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required)
    });
  }

  login() {
    this.user = new LoginModel(this.form.value.peu, this.form.value.password);
    this.loginService.sendUser(this.user)
      .subscribe(
        (user) => {
          if (user) {
            this.router.navigate(['/']).then(console.log);
          }
        }
      );
  }
}
