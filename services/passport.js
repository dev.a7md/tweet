const passport = require('passport');
const localStrategy = require('passport-local').Strategy;

const User = require('../models/userModel');

passport.serializeUser((user, done) => {
  console.log('serialize', user);
  done(null, user.id)
});
passport.deserializeUser(async (id, done) => {
  const user = await User.findById(id);
  console.log('deserialize', user);
  done(null, user);
});

const option = {
  usernameField: 'peu'
};

const strategy = new localStrategy(option, async function (peu, password, done) {
    try {
      const user = await User.findOne({username: peu});
      if (!user) {
        return done(null, false, {message: 'Incorrect Peu'});
      }

      user.comparePassword(password, (err, isMatch) => {
        if (err) {
          return done(err, false);
        }
        if (!isMatch) {
          return done(null, false, {message: 'Incorrect Password'})
        }
        return done(null, user);
      });
    } catch (err) {
      return done(err);
    }
  }
);

passport.use(strategy);
